let AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});

let ec2 = new AWS.EC2({apiVersion: '2016-11-15'});

var params = {
    DryRun: false
}

ec2.describeInstances(params, function(err,data){
    if (err){
        console.log("error",err.stack)
    }
    else {
        console.log('Sucess',JSON.stringify(data))
    }
})